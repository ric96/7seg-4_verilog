`define ZERO 8'b00111111
`define ONE 8'b00000110
`define TWO 8'b01011011
`define THREE 8'b01001111
`define FOUR 8'b01100110
`define FIVE 8'b01101101
`define SIX 8'b01111101
`define SEVEN 8'b00000111
`define EIGHT 8'b01111111
`define NINE 8'b01101111
`define DOT 8'b10000000

module top (
	output DIG1,
	output DIG2,
	output DIG3,
	output DIG4,

	output SEGA,
	output SEGB,
	output SEGC,
	output SEGD,

	output SEGE,
	output SEGF,
	output SEGG,
	output SEGDP,

	input clk

);

	reg [3:0] digit = 4'b1000;
	reg [7:0] seg1;
	reg [7:0] seg2;
	reg [7:0] seg3;
	reg [7:0] seg4;
	reg [7:0] seg;
	reg [21:0] slow_clk;
	reg [3:0] flag1, flag2, flag3, flag4;

	always @(posedge clk) begin
		digit <= digit + digit + digit[3:3];
		case(digit)
			4'b0001 : seg <= seg2;
			4'b0010 : seg <= seg3;
			4'b0100 : seg <= seg4;
			4'b1000 : seg <= seg1;
		endcase
		slow_clk <= slow_clk + 1;
	end

	always @(posedge slow_clk[21:21]) begin
		flag1 <= flag1 + 1;
		case (flag1)
			0 : 
			begin
			    seg1 <= `ZERO;
			end

			1 : 
			begin
			    seg1 <= `ONE;
			end

			2 : 
			begin
			    seg1 <= `TWO;
			end

			3 :
			begin
			    seg1 <= `THREE;
			end

			4 : 
			begin
			    seg1 <= `FOUR;
			end

			5 : 
			begin
			    seg1 <= `FIVE;
			end

			6 : 
			begin
			    seg1 <= `SIX;

			end

			7 : 
			begin
			    seg1 <= `SEVEN;
			end

			8 : 
			begin
			    seg1 <= `EIGHT;
			end

			9 : 
			begin
			    seg1 <= `NINE;
			    flag2 <= flag2 + 1;
			    if(flag2==9) begin
				flag3 <= flag3 + 1;
				flag2 <= 0;
			    end
			    if(flag2==9 && flag3==9) begin
				flag4 <= flag4 + 1;
				flag3 <= 0;
			    end
			    if(flag3==9 && flag4==9) begin
				//flag4 <= flag4 + 1;
				flag4 <= 0;
			    end
			    flag1 <= 0;
			end

		endcase

		case (flag2)
			0 : 
			begin
			    seg2 <= `ZERO;
			end

			1 : 
			begin
			    seg2 <= `ONE;
			end

			2 : 
			begin
			    seg2 <= `TWO;
			end

			3 :
			begin
			    seg2 <= `THREE;
			end

			4 : 
			begin
			    seg2 <= `FOUR;
			end

			5 : 
			begin
			    seg2 <= `FIVE;
			end

			6 : 
			begin
			    seg2 <= `SIX;

			end

			7 : 
			begin
			    seg2 <= `SEVEN;
			end

			8 : 
			begin
			    seg2 <= `EIGHT;
			end

			9 : 
			begin
			    seg2 <= `NINE;
			    //flag3 <= flag3 + 1;
			    //flag2 <= 0;
			end

		endcase
		case (flag3)
			0 : 
			begin
			    seg3 <= `ZERO;
			end

			1 : 
			begin
			    seg3 <= `ONE;
			end

			2 : 
			begin
			    seg3 <= `TWO;
			end

			3 :
			begin
			    seg3 <= `THREE;
			end

			4 : 
			begin
			    seg3 <= `FOUR;
			end

			5 : 
			begin
			    seg3 <= `FIVE;
			end

			6 : 
			begin
			    seg3 <= `SIX;

			end

			7 : 
			begin
			    seg3 <= `SEVEN;
			end

			8 : 
			begin
			    seg3 <= `EIGHT;
			end

			9 : 
			begin
			    seg3 <= `NINE;
			    //flag4 <= flag4 + 1;
			    //flag3 <= 0;
			end

		endcase

		case (flag4)
			0 : 
			begin
			    seg4 <= `ZERO;
			end

			1 : 
			begin
			    seg4 <= `ONE;
			end

			2 : 
			begin
			    seg4 <= `TWO;
			end

			3 :
			begin
			    seg4 <= `THREE;
			end

			4 : 
			begin
			    seg4 <= `FOUR;
			end

			5 : 
			begin
			    seg4 <= `FIVE;
			end

			6 : 
			begin
			    seg4 <= `SIX;

			end

			7 : 
			begin
			    seg4 <= `SEVEN;
			end

			8 : 
			begin
			    seg4 <= `EIGHT;
			end

			9 : 
			begin
			    seg4 <= `NINE;
			    flag4 <= 0;
			end

		endcase		
	end

	assign {DIG4,DIG3,DIG2,DIG1} = ~digit;
	assign {SEGDP,SEGG,SEGF,SEGE,SEGD,SEGC,SEGB,SEGA} = seg;

endmodule
